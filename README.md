# Test technique Samuel Keller pour formation cda #

https://gitlab.com/simplonlyon/selection-cda

---

## Installation ##

- Créer fichier .ENV avec 
    - MYSQL_USER (Nom d'utilisateur pour connexion SQL)
    - MYSQL_PASSWORD (Mot de passe utilisateur pour connexion SQL)
    - DB_port (Port sur lequel fonctionne l'API, défaut 8080)
    - DB_NAME (Nom de la base de donnée SQL)
- Créer une base de donnée SQL (CREATE DATABASE IF NOT EXISTS 'DB_NAME')
- npm i

## Usage ##

Cette api REST Node.js / Typescript expose plusieurs services 
- ##### Posts ######
    - /api/posts/ GET : Récupères tous les posts.
    - /api/posts/ POST : Ajoute un nouveau post, nécessite un corps de requête contenant **content**, **author**, **topicId**, une variable **date** est possible.
    - /api/posts/:id GET : Récupères un post par son id.
    - /api/posts/:id PUT : Modifie un post, nécessite un corps de requête contenant **content**, **author**, **topicId**, une variable **date** est possible.
    - /api/posts/:id DELETE Supprime un post.
- ##### Topic ######
    - /api/topics/ GET : Récupères tous les topics.
    - /api/topics/ POST : Ajoute un nouveau topic, nécessite un corps de requête contenant **title**.
    - /api/topics/:id GET : Récupères un topic par son id.
    - /api/topics/:id PUT : Modifie un topic, nécessite un corps de requête contenant **title** en plus de l'id dans l'URL.
    - /api/topics/:id DELETE Supprime un topic. Ne fonctionne que si aucun post ne fait plus référence à ce topic.


## Build ##

- dev => npm run dev
- build => npm run build