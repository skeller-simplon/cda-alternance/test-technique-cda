export const convertToInteger = (str: string) => {
    return parseInt(str, 10) || null;
}

/**
 * Middleware vérifiant que certaines variables du corps d'une requête sont belles et bien présentes.
 * Si cela n'est pas le cas, renvoie une erreur explicite.
 * @param arr Liste des variables du body devant être présentes.
 */
export const checkRequestBody = (arr: string[]) => {
    return (req: any, res: any, next: any) => {
        const missingParams = [];
        for (const variableToCheck of arr) {
            // tslint:disable-next-line:no-eval
            if (!eval( `req.body.${variableToCheck}`)) {
                missingParams.push(variableToCheck);
            }
        }
        if (missingParams.length === 0) {
            next();
        } else {
        throw new Error("Parametre(s) manquant: " + missingParams.join(","))
        }
    }
}