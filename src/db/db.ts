import dotenv from 'dotenv';
dotenv.config()
import mysql from 'mysql';


const con = mysql.createConnection({
  host: "localhost",
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database : process.env.DB_NAME
});

// Création si la database n'existe pas.

con.connect((err: Error) => {
  if (err) throw err;

  // Création de la table topic si elle n'existe pas
  con.query("SHOW TABLES LIKE 'Topic'", (error, results) => {
    if(error) return console.error(error);
    if (!results.length){
      console.log("Création de la table Topic")
      con.query(`CREATE TABLE Topic (id INT NOT NULL AUTO_INCREMENT,title VARCHAR(100) NULL, PRIMARY KEY (id), UNIQUE INDEX id_UNIQUE (id ASC));`);
    }
  });

  // Création de la table post si elle n'existe pas
  con.query("SHOW TABLES LIKE 'Post'", (error, results) => {
    if(error) return console.log(error);
    if (!results.length){
      console.log("Création de la table Post")
      con.query(`CREATE TABLE Post (id INT UNSIGNED NOT NULL AUTO_INCREMENT, content VARCHAR(100) NULL, author VARCHAR(100) NULL, date DATETIME NULL, topicId INT NOT NULL, PRIMARY KEY (id), INDEX fk_Post_1_idx (topicId ASC), CONSTRAINT fk_Post_1 FOREIGN KEY (topicId) REFERENCES Topic (id) ON DELETE NO ACTION ON UPDATE NO ACTION)`);
    }
  });
});

export default con