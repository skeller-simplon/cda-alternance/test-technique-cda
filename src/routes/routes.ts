import express from 'express'
const router = express()

import TopicController from "./TopicController";
import PostController from "./PostController";


router.use('/topics', TopicController);
router.use('/posts', PostController);

export default router;
