import express from "express";
import con from "../db/db"
import { convertToInteger, checkRequestBody } from "../services/services"
const PostController = express.Router();

// Récupères la liste de tous les posts
PostController.route("/").get((req, res) => {
    con.query('SELECT * FROM Post', (error, posts) => {
        res.send(posts)
    });
// Création d'un post, date facultative dans la requête.
}).post(checkRequestBody(["content", "author", "topicId"]), (req, res) => {
    // Ajoute une date si elle est manquante
    if(!req.body.date) req.body.date = new Date();

    con.query(`INSERT INTO Post (content, author, date, topicId) VALUES (?, ?, ?, ?)`,
        [req.body.content, req.body.author, req.body.date, req.body.topicId],
        (error, post) => {
            if(error) res.status(400).send(error);
            else res.send("Created.")
    });
})
// Récupération du post par son id.
PostController.route("/:id").get((req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else
        con.query(`SELECT * FROM Post where id = ? LIMIT 1`, [idParam], (error, post) => {
            if(error) res.status(400).send(error);
            else res.send(post[0])
        });
// Modification d'un post, date facultative dans la requête.
}).put(checkRequestBody(["content", "author", "topicId"]), (req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else {
        con.query(`UPDATE Post SET content=?, author=?, topicId=? WHERE id=?`,
        [req.body.content, req.body.author, req.body.topicId, idParam], (error: any, result: any) => {
            if(error) res.status(400).send(error);
            res.send(result.message)
        });
    }
// Supression d'un post.
}).delete((req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else {
        con.query(`DELETE FROM Post WHERE id=?`, [idParam],
        (error: any, result: any) => {
            if(error) res.status(400).send(error);
            res.send(result.message)
        });
    }
})

export default PostController;