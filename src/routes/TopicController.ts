import express from "express";
import con from "../db/db"
import { convertToInteger, checkRequestBody } from "../services/services"

const TopicController = express.Router();

TopicController.route("/").get((req, res) => {
    con.query('SELECT * FROM Topic', (error, result) => {
        res.send(result)
    });
// Création d'un topic.
}).post(checkRequestBody(["title"]), (req, res) => {
    con.query(`INSERT INTO Topic (title) VALUES (?)`,
        [req.body.title],
        (error, result) => {
            if(error) res.status(400).send(error);
            else res.send("Created.")
    });
})

// Récupération du topic par son id.
TopicController.route("/:id").get((req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else
        con.query(`SELECT * FROM Topic where id = ? LIMIT 1`, [idParam], (error, results) => {
            if(error) res.status(400).send(error);
            else res.send(results[0])
        });
// Modification d'un topic, date facultative dans la requête.
}).put(checkRequestBody(["title"]), (req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else {
        con.query(`UPDATE Topic SET title=? WHERE id=?`,
        [req.body.title, idParam], (error: any, result: any) => {
            if(error) res.status(400).send(error);
            res.send(result.message)
        });
    }
// Supression d'un topic.
}).delete((req, res) => {
    const idParam: number = convertToInteger(req.params.id);
    if(!idParam) res.status(400).send("Wrong identifier.")
    else {
        con.query("SELECT * FROM Post WHERE topicId = ?", [idParam],
        (error: any, result: any) => {
            // Si le topic contient encore des posts, on ne le supprime pas.
            if(result.length > 0)
                res.status(400).send("Unable to delete topic, it contains posts.")
            // Sinon, on le supprime.
            else
                con.query(`DELETE FROM Topic WHERE id=?`, [idParam],
                (err: any, resul: any) => {
                    if(err) res.status(400).send(error);
                    res.send(resul.message)
                });
        })
    }
})


export default TopicController;