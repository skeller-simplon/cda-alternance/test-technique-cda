import express from "express";
import routes from "./routes/routes";
import dotenv from 'dotenv';
import bodyParser from 'body-parser';

dotenv.config();
const app = express();
const port = 8080 || process.env.DB_port;

// Bodyparser - traite les requêtes.
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

app.get("/", (req, res) => {
  res.send("Hi!");
});

app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Serveur sur port ${port}`);
});

app.use("/api", routes);
